use crate::cube2::Cube;
use crate::cube2::FaceRotation;
use crate::cube2::strum::IntoEnumIterator;


// maximum number of move to solve any scramble
const GOD_NUMBER: u8 = 25;

const MAX_NB_NODES_SCAMBLED_SIDE: usize = 69984; // 9 * 6 ^ 5
const MAX_NB_NODES_SOLVED_SIDE: usize = 11664; // 9 * 6 ^ 4

#[derive(Clone)]
struct Node {
    state: Cube,
    previous_moves: Vec<FaceRotation>,
}

pub fn solve(input: Cube) -> String {
    let mut scrambled_side: Vec<Node> = Vec::with_capacity(MAX_NB_NODES_SCAMBLED_SIDE);
    let mut solved_side: Vec<Node> = Vec::with_capacity(MAX_NB_NODES_SOLVED_SIDE);

    let solved_cube = Cube::new();
    let first_solved_node = Node {
        state: solved_cube,
        previous_moves: vec![FaceRotation::None],
    };
    solved_side.push(first_solved_node);

    let first_scrambled_node = Node {
        state: input,
        previous_moves: vec![FaceRotation::None],
    };
    scrambled_side.push(first_scrambled_node);

    // to write and then append to the corresponding side
    let mut current_nodes: Vec<Node> = Vec::new();
    let mut is_scrambled_side = true;

    for _ in 0..GOD_NUMBER {
        let current_side: &Vec<Node>;
        let other_side: &Vec<Node>;
        if is_scrambled_side {
            current_side = &scrambled_side;
            other_side = &solved_side;
        } else {
            current_side = &solved_side;
            other_side = &scrambled_side;
        }
        
        for i in 0..current_side.len() {

            for rotation in FaceRotation::iter() {
                if Cube::rotation_is_redundant(&rotation, current_side[i].previous_moves.last()) {
                    continue;
                }
                let mut new_state = current_side[i].state.copy();
                new_state.apply_move(rotation);

                let mut new_vec = current_side[i].previous_moves.clone();
                new_vec.push(rotation);
                let new_node = Node {
                    state: new_state.clone(),
                    previous_moves: new_vec,
                };

                for node in other_side.iter() {
                    if new_state.is_equal(&node.state) {
                        println!("Found a solution!");

                        if is_scrambled_side {
                            return print_solution(new_node, node.clone());
                        } else {
                             return print_solution(node.clone(), new_node);
                        }
                    }
                }
                current_nodes.push(new_node);
            }
        }

        if is_scrambled_side {
            scrambled_side.extend_from_slice(&current_nodes);
        } else {
            solved_side.extend_from_slice(&current_nodes);
        }
        current_nodes.clear();
        is_scrambled_side = ! is_scrambled_side;
    }

    String::from("Solve end (failure)")
}

fn print_solution(scrambled_side_node: Node, solved_side_node: Node) -> String {
    let mut output = String::new();
    let mut nb_rotations = 0;

    for rotation in scrambled_side_node.previous_moves.iter() {
        if *rotation == FaceRotation::None {
            continue;
        }
        output.push_str(&Cube::rotation_to_string(*rotation));
        output.push_str(" ");
        nb_rotations += 1;
    }

    let mut reversed = solved_side_node.previous_moves.clone();
    reversed.reverse();

    for rotation in reversed.iter() {
        if *rotation == FaceRotation::None {
            continue;
        }
        output.push_str(&Cube::rotation_to_string(Cube::reverse_rotation(*rotation)));
        output.push_str(" ");
        nb_rotations += 1;
    }

    println!("There's {} rotations", nb_rotations);

    output
}
