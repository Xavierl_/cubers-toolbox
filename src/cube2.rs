// represents a 2x2 cube and the modifications that can be done to it
pub extern crate strum;
extern crate strum_macros;
use strum_macros::EnumIter;

const NB_PIECES: u8 = 7;

#[derive(PartialEq, EnumIter, Clone, Copy)]
pub enum FaceRotation {
    F,
    Fprime,
    F2,
    R,
    Rprime,
    R2,
    U,
    Uprime,
    U2,
    None,
}

// the axis the white or yellow face is aligned with
#[derive(PartialEq, Clone, Copy)]
enum PieceOrientation {
    F,
    R,
    U,
}

// one of the 8 pieces a 2x2 cube contains
#[derive(Clone)]
struct Piece {
    number: u8,
    orientation: PieceOrientation,
}

// represents a 2x2 cube, the piece on BDL is omitted
#[derive(Clone)]
pub struct Cube {
    slots: [Piece; NB_PIECES as usize],
}

// TODO: struct Cube that knows previous moves

impl Cube {
    pub fn new() -> Cube {
        let output = Cube {
            slots: [Piece { number: 0, orientation: PieceOrientation::U },
                    Piece { number: 1, orientation: PieceOrientation::U },
                    Piece { number: 2, orientation: PieceOrientation::U },
                    Piece { number: 3, orientation: PieceOrientation::U },
                    Piece { number: 4, orientation: PieceOrientation::U },
                    Piece { number: 5, orientation: PieceOrientation::U },
                    Piece { number: 6, orientation: PieceOrientation::U },],
        };
    
        output
    }

    pub fn copy(&self) -> Cube {
        let copy = Cube {
            slots: [Piece { number: self.slots[0].number, orientation: self.slots[0].orientation },
                    Piece { number: self.slots[1].number, orientation: self.slots[1].orientation },
                    Piece { number: self.slots[2].number, orientation: self.slots[2].orientation },
                    Piece { number: self.slots[3].number, orientation: self.slots[3].orientation },
                    Piece { number: self.slots[4].number, orientation: self.slots[4].orientation },
                    Piece { number: self.slots[5].number, orientation: self.slots[5].orientation },
                    Piece { number: self.slots[6].number, orientation: self.slots[6].orientation },],
        };

        copy
    }

    pub fn parse_scramble(input: &str) -> Cube {
        let mut c = Cube::new();
        let scramble = input.as_bytes();

        for i in 0..input.len() {

            if scramble[i] != b'F' && scramble[i] != b'R' && scramble[i] != b'U' {
                continue;
            }

            if scramble[i] == b'F' {
                if i + 1 < scramble.len() && scramble[i+1] == b'\'' {
                    c.apply_move(FaceRotation::Fprime);

                } else if i + 1 < scramble.len() && scramble[i+1] == b'2' {
                    c.apply_move(FaceRotation::F2);

                } else {
                    c.apply_move(FaceRotation::F);
                }

            } else if scramble[i] == b'R' {
                if i + 1 < scramble.len() && scramble[i+1] == b'\'' {
                    c.apply_move(FaceRotation::Rprime);

                } else if i + 1 < scramble.len() && scramble[i+1] == b'2' {
                    c.apply_move(FaceRotation::R2);

                } else {
                    c.apply_move(FaceRotation::R);
                }

            } else if scramble[i] == b'U' {
                if i + 1 < scramble.len() && scramble[i+1] == b'\'' {
                    c.apply_move(FaceRotation::Uprime);

                } else if i + 1 < scramble.len() && scramble[i+1] == b'2' {
                    c.apply_move(FaceRotation::U2);

                } else {
                    c.apply_move(FaceRotation::U);
                }
            }
        }

        c
    }

    pub fn apply_move(&mut self, rotation: FaceRotation) {
        match rotation {
            FaceRotation::F => self.move_f(),
            FaceRotation::Fprime => self.move_fprime(),
            FaceRotation::F2 => self.move_f2(),
            FaceRotation::R => self.move_r(),
            FaceRotation::Rprime => self.move_rprime(),
            FaceRotation::R2 => self.move_r2(),
            FaceRotation::U => self.move_u(),
            FaceRotation::Uprime => self.move_uprime(),
            FaceRotation::U2 => self.move_u2(),
            _ => (),
        };
    }

    fn move_f(&mut self) {
        let mut temp1 = Piece {
            number: self.slots[2].number,
            orientation: self.slots[2].orientation,
        };
        
        let temp2 = Piece {
            number: self.slots[3].number,
            orientation: self.slots[3].orientation,
        };
    
        self.slots[2].number = self.slots[4].number;
        self.slots[2].orientation = Self::switch_orientation_f(self.slots[4].orientation);
        
        self.slots[3].number = temp1.number;
        self.slots[3].orientation = Self::switch_orientation_f(temp1.orientation);

        temp1.number = self.slots[5].number;
        temp1.orientation = self.slots[5].orientation;
    
        self.slots[5].number = temp2.number;
        self.slots[5].orientation = Self::switch_orientation_f(temp2.orientation);
    
        self.slots[4].number = temp1.number;
        self.slots[4].orientation = Self::switch_orientation_f(temp1.orientation);
    }
    
    fn move_fprime(&mut self) {
        let mut temp1 = Piece {
            number: self.slots[5].number,
            orientation: self.slots[5].orientation,
        };
        
        let temp2 = Piece {
            number: self.slots[3].number,
            orientation: self.slots[3].orientation,
        };

        self.slots[5].number = self.slots[4].number;
        self.slots[5].orientation = Self::switch_orientation_f(self.slots[4].orientation);

        self.slots[3].number = temp1.number;
        self.slots[3].orientation = Self::switch_orientation_f(temp1.orientation);

        temp1.number = self.slots[2].number;
        temp1.orientation = self.slots[2].orientation;

        self.slots[2].number = temp2.number;
        self.slots[2].orientation = Self::switch_orientation_f(temp2.orientation);

        self.slots[4].number = temp1.number;
        self.slots[4].orientation = Self::switch_orientation_f(temp1.orientation);
    }
    
    fn move_f2(&mut self) {
        let mut temp = Piece {
            number: self.slots[5].number,
            orientation: self.slots[5].orientation,
        };
        self.slots[5].number = self.slots[2].number;
        self.slots[5].orientation = self.slots[2].orientation;
        self.slots[2].number = temp.number;
        self.slots[2].orientation = temp.orientation;

        temp.number = self.slots[4].number;
        temp.orientation = self.slots[4].orientation;
        self.slots[4].number = self.slots[3].number;
        self.slots[4].orientation = self.slots[3].orientation;
        self.slots[3].number = temp.number;
        self.slots[3].orientation = temp.orientation;
    }

    fn switch_orientation_f(orientation: PieceOrientation) -> PieceOrientation {
        match orientation {
            PieceOrientation::F => PieceOrientation::F,
            PieceOrientation::R => PieceOrientation::U,
            PieceOrientation::U => PieceOrientation::R,
        }
    }

    fn move_r(&mut self) {
        let mut temp1 = Piece {
            number: self.slots[3].number,
            orientation: self.slots[3].orientation,
        };
        
        let temp2 = Piece {
            number: self.slots[1].number,
            orientation: self.slots[1].orientation,
        };
    
        self.slots[3].number = self.slots[5].number;
        self.slots[3].orientation = Self::switch_orientation_r(self.slots[5].orientation);
        
        self.slots[1].number = temp1.number;
        self.slots[1].orientation = Self::switch_orientation_r(temp1.orientation);

        temp1.number = self.slots[6].number;
        temp1.orientation = self.slots[6].orientation;
    
        self.slots[6].number = temp2.number;
        self.slots[6].orientation = Self::switch_orientation_r(temp2.orientation);
    
        self.slots[5].number = temp1.number;
        self.slots[5].orientation = Self::switch_orientation_r(temp1.orientation);
    }
    
    fn move_rprime(&mut self) {
        let mut temp1 = Piece {
            number: self.slots[6].number,
            orientation: self.slots[6].orientation,
        };
        
        let temp2 = Piece {
            number: self.slots[1].number,
            orientation: self.slots[1].orientation,
        };

        self.slots[6].number = self.slots[5].number;
        self.slots[6].orientation = Self::switch_orientation_r(self.slots[5].orientation);

        self.slots[1].number = temp1.number;
        self.slots[1].orientation = Self::switch_orientation_r(temp1.orientation);

        temp1.number = self.slots[3].number;
        temp1.orientation = self.slots[3].orientation;

        self.slots[3].number = temp2.number;
        self.slots[3].orientation = Self::switch_orientation_r(temp2.orientation);

        self.slots[5].number = temp1.number;
        self.slots[5].orientation = Self::switch_orientation_r(temp1.orientation);
    }
    
    fn move_r2(&mut self) {
        let mut temp = Piece {
            number: self.slots[6].number,
            orientation: self.slots[6].orientation,
        };
        self.slots[6].number = self.slots[3].number;
        self.slots[6].orientation = self.slots[3].orientation;
        self.slots[3].number = temp.number;
        self.slots[3].orientation = temp.orientation;

        temp.number = self.slots[5].number;
        temp.orientation = self.slots[5].orientation;
        self.slots[5].number = self.slots[1].number;
        self.slots[5].orientation = self.slots[1].orientation;
        self.slots[1].number = temp.number;
        self.slots[1].orientation = temp.orientation;
    }

    fn switch_orientation_r(orientation: PieceOrientation) -> PieceOrientation {
        match orientation {
            PieceOrientation::F => PieceOrientation::U,
            PieceOrientation::R => PieceOrientation::R,
            PieceOrientation::U => PieceOrientation::F,
        }
    }

    fn move_u(&mut self) {
        let mut temp1 = Piece {
            number: self.slots[0].number,
            orientation: self.slots[0].orientation,
        };
        
        let temp2 = Piece {
            number: self.slots[1].number,
            orientation: self.slots[1].orientation,
        };
    
        self.slots[0].number = self.slots[2].number;
        self.slots[0].orientation = Self::switch_orientation_u(self.slots[2].orientation);
        
        self.slots[1].number = temp1.number;
        self.slots[1].orientation = Self::switch_orientation_u(temp1.orientation);

        temp1.number = self.slots[3].number;
        temp1.orientation = self.slots[3].orientation;
    
        self.slots[3].number = temp2.number;
        self.slots[3].orientation = Self::switch_orientation_u(temp2.orientation);
    
        self.slots[2].number = temp1.number;
        self.slots[2].orientation = Self::switch_orientation_u(temp1.orientation);
    }
    
    fn move_uprime(&mut self) {
        let mut temp1 = Piece {
            number: self.slots[3].number,
            orientation: self.slots[3].orientation,
        };
        
        let temp2 = Piece {
            number: self.slots[1].number,
            orientation: self.slots[1].orientation,
        };

        self.slots[3].number = self.slots[2].number;
        self.slots[3].orientation = Self::switch_orientation_u(self.slots[2].orientation);

        self.slots[1].number = temp1.number;
        self.slots[1].orientation = Self::switch_orientation_u(temp1.orientation);

        temp1.number = self.slots[0].number;
        temp1.orientation = self.slots[0].orientation;

        self.slots[0].number = temp2.number;
        self.slots[0].orientation = Self::switch_orientation_u(temp2.orientation);

        self.slots[2].number = temp1.number;
        self.slots[2].orientation = Self::switch_orientation_u(temp1.orientation);
    }
    
    fn move_u2(&mut self) {
        let mut temp = Piece {
            number: self.slots[3].number,
            orientation: self.slots[3].orientation,
        };
        self.slots[3].number = self.slots[0].number;
        self.slots[3].orientation = self.slots[0].orientation;
        self.slots[0].number = temp.number;
        self.slots[0].orientation = temp.orientation;

        temp.number = self.slots[2].number;
        temp.orientation = self.slots[2].orientation;
        self.slots[2].number = self.slots[1].number;
        self.slots[2].orientation = self.slots[1].orientation;
        self.slots[1].number = temp.number;
        self.slots[1].orientation = temp.orientation;
    }

    fn switch_orientation_u(orientation: PieceOrientation) -> PieceOrientation {
        match orientation {
            PieceOrientation::F => PieceOrientation::R,
            PieceOrientation::R => PieceOrientation::F,
            PieceOrientation::U => PieceOrientation::U,
        }
    }

    pub fn is_equal(&self, c2: &Cube) -> bool {
        self.slots[0].number == c2.slots[0].number &&
        self.slots[0].orientation == c2.slots[0].orientation &&
        self.slots[1].number == c2.slots[1].number &&
        self.slots[1].orientation == c2.slots[1].orientation &&
        self.slots[2].number == c2.slots[2].number &&
        self.slots[2].orientation == c2.slots[2].orientation &&
        self.slots[3].number == c2.slots[3].number &&
        self.slots[3].orientation == c2.slots[3].orientation &&
        self.slots[4].number == c2.slots[4].number &&
        self.slots[4].orientation == c2.slots[4].orientation &&
        self.slots[5].number == c2.slots[5].number &&
        self.slots[5].orientation == c2.slots[5].orientation &&
        self.slots[6].number == c2.slots[6].number &&
        self.slots[6].orientation == c2.slots[6].orientation
    }

    pub fn to_string(&self) -> String {
        let output = format!("{}{}\n{}{}\n{}{}{}",
                         self.piece_to_string(0),
                         self.piece_to_string(1),
                         self.piece_to_string(2),
                         self.piece_to_string(3),
                         self.piece_to_string(4),
                         self.piece_to_string(5),
                         self.piece_to_string(6),
            );
        output
    }
    
    fn piece_to_string(&self, n: u8) -> String {
        let output = self.slots[n as usize].number.to_string();
    
        match self.slots[n as usize].orientation {
            PieceOrientation::F => format!("{}{}", output, "F"),
            PieceOrientation::R => format!("{}{}", output, "R"),
            PieceOrientation::U => format!("{}{}", output, "U"),
        }
    }

    pub fn reverse_rotation(rotation: FaceRotation) -> FaceRotation {
        match rotation {
            FaceRotation::F => return FaceRotation::Fprime,
            FaceRotation::Fprime => return FaceRotation::F,
            FaceRotation::R => return FaceRotation::Rprime,
            FaceRotation::Rprime => return FaceRotation::R,
            FaceRotation::U => return FaceRotation::Uprime,
            FaceRotation::Uprime => return FaceRotation::U,
            _ => return rotation,
        };
    }

    pub fn rotation_to_string(rotation: FaceRotation) -> &'static str {
        match rotation {
            FaceRotation::F => return "F",
            FaceRotation::Fprime => return "F'",
            FaceRotation::F2 => return "F2",
            FaceRotation::R => return "R",
            FaceRotation::Rprime => return "R'",
            FaceRotation::R2 => return "R2",
            FaceRotation::U => return "U",
            FaceRotation::Uprime => return "U'",
            FaceRotation::U2 => return "U2",
            _ => return "🔥 INVALID ROTATION!",
        };
    }

    pub fn rotation_is_redundant(current: &FaceRotation, previous: Option<&FaceRotation>) -> bool {
        if previous == None {
            return false;
        }

        let p = previous.unwrap();

        match current {
            FaceRotation::F => if *p == FaceRotation::Fprime || *p == FaceRotation::F2 {return true;},
            FaceRotation::Fprime => if *p == FaceRotation::F || *p == FaceRotation::F2 {return true;},
            FaceRotation::F2 => if *p == FaceRotation::Fprime || *p == FaceRotation::F {return true;},
            FaceRotation::R => if *p == FaceRotation::Rprime || *p == FaceRotation::R2 {return true;},
            FaceRotation::Rprime => if *p == FaceRotation::R || *p == FaceRotation::R2 {return true;},
            FaceRotation::R2 => if *p == FaceRotation::Rprime || *p == FaceRotation::R {return true;},
            FaceRotation::U => if *p == FaceRotation::Uprime || *p == FaceRotation::U2 {return true;},
            FaceRotation::Uprime => if *p == FaceRotation::U || *p == FaceRotation::U2 {return true;},
            FaceRotation::U2 => if *p == FaceRotation::Uprime || *p == FaceRotation::U {return true;},
            _ => return false,
        };
        false
    }
}
