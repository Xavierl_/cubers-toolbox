use std::io;

mod solver;
mod cube2;
use crate::cube2 as cube2x2;

fn main() {
    println!("Please enter your scramble using F, U and R:");

    let mut buffer = String::new();
    let _ = io::stdin().read_line(&mut buffer);
    // TODO: do a to upper to buffer
    
    let scrambled_cube = cube2x2::Cube::parse_scramble(&buffer);
    println!("Your cube looks like this:");
    println!("{}", scrambled_cube.to_string());

    let out = solver::solve(scrambled_cube);
    println!("{}", out);
}