# Cubers' toolbox
A set of tool related to Rubik's cubes. The goal is to make it accessible from any web browser.

## What's in the toolbox?
### Scambler ⚙️
The scrambler should be able to make scrambles for all common Rubik's cube puzzles. The scrambles must be sovable with a minimum amount of moves, depending on the puzzle. That's why the scambler will use the next tool in the toolbox.

### Solver ⚙️
The solver provides a set of move to perform for solving a given scramble or cube state. It gives a solution with the lowest number of rotations by applying moves both from the solved state and from the solved state and comparing the 2 sides. Once it has found an equivalent state in both sides it gives the solution by reversing the solved side moves and adding them to the end of the scrambled side moves. To check if a scramble is solvable with at least 5 moves, it can explore with a depth of 2 from both sides and check if there is a match.
 - ✅ 2x2
 - ⚙️ 3x3

### FMC challenge
Fewest move count (FMC) is an event where competitors are given the same scramble and should find the shortest solution within 1 hour, with no external help. The toolbox should make it easy to organise such events online.

### Timer
It should give several user the same scramble and keep the data to make a leaderboard during a competition for example.
